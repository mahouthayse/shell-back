const { Joi } = require('express-validation');

const StoreValidation = {
    body: Joi.object({
        name: Joi.string().required(),
        email: Joi.string().email().required(),
        password: Joi.string().min(6).required()
    })
};

const LoginValidation = {
    body: Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
    })
};

const UpdateValidation = {
    params: Joi.object({
        id: Joi.string().alphanum().length(24).required()
    }),
    body: Joi.object({
        name: Joi.string().optional(),
        email: Joi.string().email().optional(),
        password: Joi.string().min(6).optional()
    })
}

const RemoveValidation = {
    params: Joi.object({
        id: Joi.string().alphanum().length(24).required()
    })
}

module.exports = {
    StoreValidation,
    LoginValidation,
    UpdateValidation,
    RemoveValidation
};