const mongoose = require('mongoose');
const User = mongoose.model('User');

class UserController {
    async index(req, res, next) {
        try {
            const users = await User.find().select('_id name email');
            return res.json(users);
        } catch (error) {
            next(error);
        }
    }

    async store(req, res, next) {
        const { name, email, password } = req.body;

        try {
            const user = new User({ name, email, password });
            user.setPassword(password);
            await user.save();
            return res.json({ user: user.sendAuthJSON() });
        } catch (error) {
            next(error);
        }
    }

    async update(req, res, next) {
        const { name, email, password } = req.body;
        try {
            const user = await User.findById(req.params.id);
            if (!user) return res.status(401).json({ error: 'Usuário não registrado' });
            if (typeof name !== 'undefined') user.name = name;
            if (typeof email !== 'undefined') user.email = email;
            if (typeof password !== 'undefined') user.setPassword(password);
            await user.save();
            return res.json({ user: user.sendAuthJSON() });
        } catch (error) {
            console.log(error);
            next(error);
        }   
    }

    async remove(req, res, next) {
        try {
            const user = await User.findById(req.params.id);
            if (!user) return res.status(401).json({ error: 'Usuário não registrado' });
            await user.remove();
            return res.json({ deleted: true });
        } catch (error) {
            console.log(error);
            next(error);
        }
    }

    async login(req, res, next) {
        const { email, password } = req.body;
        try {
            const user = await User.findOne({ email });
            if (!user) return res.status(404).json({ error: 'Usuário não registrado' });
            if (!user.validatePassword(password)) return res.status(401).json({ error: 'Senha inválida' });
            return res.json({ user: user.sendAuthJSON() });
        } catch (error) {
            next(error);
        }
    }
}

module.exports = UserController;