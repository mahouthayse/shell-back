const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const uniqueValidator = require('mongoose-unique-validator');
const secret = require('../config').secret;

const User = mongoose.Schema({
    name: { type: String, required: true },
    email: {
        type: String,
        required: true,
        lowercase: true,
        unique: true,
        match: [/\S+@\S+\.\S+/, 'é inválido.']
    },
    hash: String,
    salt: String,
}, { timestamps: true });

User.plugin(uniqueValidator, { message: 'Já está sendo utilizado' });

User.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

User.methods.validatePassword = function (password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return hash === this.hash;
};

User.methods.generateToken = function () {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 1);

    return jwt.sign({
        id: this._id,
        email: this.email,
        exp: parseFloat(exp.getTime() / 1000, 10)
    }, secret);
};

User.methods.sendAuthJSON = function () {
    return {
        _id: this.id,
        name: this.name,
        email: this.email,
        role: this.roles,
        token: this.generateToken()
    };
};

// recovery password methods

module.exports = mongoose.model('User', User);