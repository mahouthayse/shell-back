const router = require('express').Router();

router.use('/v1/api', require('./api/v1'));

module.exports = router;