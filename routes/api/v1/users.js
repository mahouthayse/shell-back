const router = require('express').Router();
const { validate } = require('express-validation');

const UserController = require('../../../controllers/UserController');
const { StoreValidation, LoginValidation, UpdateValidation, RemoveValidation } = require('../../../controllers/validations/userValidation');

const userController = new UserController();

router.post('/login', validate(LoginValidation), userController.login);
router.post('/register', validate(StoreValidation), userController.store);
router.patch('/:id', validate(UpdateValidation), userController.update);
router.delete('/:id', validate(RemoveValidation), userController.remove);

router.get('/', userController.index);

module.exports = router;